package thai.word.controller;

import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("backend")
public class RestApiController {

	@RequestMapping(value = "/getProduct", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> getProduct(String word) {
		System.out.println("word: "+word);
		List<String> ls = null;
		try {
			ls = printEachWord(word);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ls;
	}
	private List<String> printEachWord(String source) throws IOException {
	    source = StringUtils.isNotBlank(source) ? source : "";
	    Locale thaiLocale = new Locale("th");
	    BreakIterator boundary = BreakIterator.getWordInstance(thaiLocale);
	    boundary.setText(source);
	    StringBuffer strout = new StringBuffer();
	    List<String> ls = new ArrayList<>();
	    int start = boundary.first();
	    for (int end = boundary.next();
	         end != BreakIterator.DONE;
	         start = end, end = boundary.next()) {
	         strout.append(source.substring(start, end)+"-");
	         ls.add(source.substring(start, end));
	    }
	    return ls;
	}
}
